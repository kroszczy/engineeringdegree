import numpy as np
import scipy.ndimage.filters as filters

import utils.utils as utils


# gft.oriented_gaussian(gimage, 1, (0, 1), 60)
def oriented_gaussian(image, sigma, order, angle_anticlockwise, mode='nearest', cval=0.0, truncate=4.0):
    rows, cols = image.shape
    rotated = utils.rotate_about_center(image, angle_anticlockwise)
    output_rotated = np.zeros(rotated.shape)
    filters.gaussian_filter(rotated, sigma, order, output_rotated, mode, cval, truncate)
    output_uncropped = utils.rotate_about_center(output_rotated, -1 * angle_anticlockwise)
    rows2, cols2 = output_uncropped.shape
    output = output_uncropped[int(np.ceil(rows2 / 2 - rows / 2)): int(np.ceil(rows2 / 2 + rows / 2)),
             int(np.ceil(cols2 / 2 - cols / 2)): int(np.ceil(cols2 / 2 + cols / 2))]
    minimum = np.amin(output)
    output = output + np.abs(minimum)
    output = np.around(output)
    return output


def gaussians_difference(image, sigma, order, angle_aclockwise):
    images = []
    if len(sigma) != len(order) or len(order) != len(angle_aclockwise) or len(sigma) != len(angle_aclockwise):
        raise ValueError("sima, order, angle_clockwise must have the same number of elements")
    for i in range(0, len(sigma)):
        images.append(oriented_gaussian(image, sigma[i], order[i], angle_aclockwise[i]))
    output_image = images[0]
    for i in range(1, len(images)):
        output_image = np.subtract(output_image, images[i])
    minimum = np.amin(output_image)
    output = output_image + np.abs(minimum)
    output = np.around(output)
    return output
