import unittest
import cv2
from opencv_ext import gaussian_filter as gf


class TestGaussianFilter(unittest.TestCase):
    def test_directed_gaussian(self):
        test_image = cv2.imread('gaussian_test_img.png')
        test_image = cv2.cvtColor(test_image, cv2.COLOR_BGR2GRAY)
        rotated = gf.oriented_gaussian(test_image, 1, (0, 1), 90)
        unrotated = gf.oriented_gaussian(test_image, 1, (1, 0), 0)
        for i in range(0, rotated.shape[0]):
            for j in range(0, rotated.shape[1]):
                self.assertAlmostEqual(rotated[i, j], unrotated[i, j], delta=1)

    def test_DoG(self):
        test_image = cv2.imread('gaussian_32.png')
        test_image = cv2.cvtColor(test_image, cv2.COLOR_BGR2GRAY)
        result = gf.gaussians_difference(test_image, [3, 1], [(0, 0), (0, 0)], [0, 0])
        cv2.imwrite('testimg.png', result)


if __name__ == '__main__':
    unittest.main()
