import numpy as np
import cv2
from copy import copy
from sklearn.cluster import k_means

class ImageChannelProcessor:
    def __init__(self, rgb_img):
        self.rgb_image = rgb_img
        self.lab_image = cv2.cvtColor(rgb_img, cv2.COLOR_BGR2LAB)

    def getRImg(self):
        result = copy(self.rgb_image)
        result[:, :, 0:2] = 0
        return result

    def getGImg(self):
        result = copy(self.rgb_image)
        result[:, :, 0] = 0
        result[:, :, 2] = 0
        return result

    def getBImg(self):
        result = copy(self.rgb_image)
        result[:, :, 1:3] = 0
        return result

    def getLImg(self):
        result = copy(self.lab_image)
        result[:, :, 1] = result[:, :, 0]
        result[:, :, 2] = result[:, :, 0]
        return result

    def getaImg(self):
        result = copy(self.lab_image)
        result[:, :, 0] = result[:, :, 1]
        result[:, :, 2] = result[:, :, 1]
        return result

    def getbImg(self):
        result = copy(self.lab_image)
        result[:, :, 0] = result[:, :, 2]
        result[:, :, 1] = result[:, :, 2]
        return result

    def getTextureImg(self, number_of_textons):
        image_to_process = cv2.cvtColor(self.rgb_image, cv2.COLOR_BGR2GRAY)
        gaussian_images = self.__multigauss_filter_image(image_to_process)
        return image_to_process

    def getBGRImg(self):
        return copy(self.rgb_image)

    def getLabImg(self):
        return copy(self.lab_image)

    def __multigauss_filter_image(self, image_to_process):
        return [
            cv2.GaussianBlur(image_to_process, )
        ]
