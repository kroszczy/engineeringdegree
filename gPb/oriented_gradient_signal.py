import cv2
import numpy as np


class OrientedGradientSignal:
    def __init__(self, image, x, y, theta):
        self.image = image
        self.x = x
        self.y = y
        self.theta = theta

