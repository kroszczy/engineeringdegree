import cv2
import numpy as np
from gPb.image_channel_processor import ImageChannelProcessor
import scipy.ndimage.filters as filters
import opencv_ext.gaussian_filter as gft
import copy
# image = cv2.imread('orig.png')
from matplotlib import pyplot as plt

image = cv2.imread('orig.png')

gimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
rows, cols = gimage.shape
cv2.imwrite('gimage.png', gimage)

fimage = gft.gaussians_difference(gimage, [3, 1], [(0, 0), (0, 0)], [0, 0])
cv2.imwrite('0.png', fimage)



cv2.waitKey(0)
cv2.destroyAllWindows()
